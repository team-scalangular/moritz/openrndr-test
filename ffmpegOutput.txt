ffmpeg version 6.0 Copyright (c) 2000-2023 the FFmpeg developers
  built with Apple clang version 14.0.3 (clang-1403.0.22.14.1)
  configuration: --prefix=/opt/homebrew/Cellar/ffmpeg/6.0 --enable-shared --enable-pthreads --enable-version3 --cc=clang --host-cflags= --host-ldflags= --enable-ffplay --enable-gnutls --enable-gpl --enable-libaom --enable-libaribb24 --enable-libbluray --enable-libdav1d --enable-libmp3lame --enable-libopus --enable-librav1e --enable-librist --enable-librubberband --enable-libsnappy --enable-libsrt --enable-libsvtav1 --enable-libtesseract --enable-libtheora --enable-libvidstab --enable-libvmaf --enable-libvorbis --enable-libvpx --enable-libwebp --enable-libx264 --enable-libx265 --enable-libxml2 --enable-libxvid --enable-lzma --enable-libfontconfig --enable-libfreetype --enable-frei0r --enable-libass --enable-libopencore-amrnb --enable-libopencore-amrwb --enable-libopenjpeg --enable-libspeex --enable-libsoxr --enable-libzmq --enable-libzimg --disable-libjack --disable-indev=jack --enable-videotoolbox --enable-neon
  libavutil      58.  2.100 / 58.  2.100
  libavcodec     60.  3.100 / 60.  3.100
  libavformat    60.  3.100 / 60.  3.100
  libavdevice    60.  1.100 / 60.  1.100
  libavfilter     9.  3.100 /  9.  3.100
  libswscale      7.  1.100 /  7.  1.100
  libswresample   4. 10.100 /  4. 10.100
  libpostproc    57.  1.100 / 57.  1.100
Input #0, rawvideo, from 'fd:':
  Duration: N/A, start: 0.000000, bitrate: 3686400 kb/s
  Stream #0:0: Video: rawvideo (RGBA / 0x41424752), rgba, 1600x1600, 3686400 kb/s, 45 tbr, 45 tbn
Stream mapping:
  Stream #0:0 -> #0:0 (rawvideo (native) -> gif (native))
[Parsed_palettegen_1 @ 0x600002cfc2c0] 255(+1) colors generated out of 251523 colors; ratio=0.001014
Output #0, gif, to 'video/TemplateProgram-2023-04-27-11.21.51.gif':
  Metadata:
    encoder         : Lavf60.3.100
  Stream #0:0: Video: gif, pal8(pc, gbr/unknown/unknown, progressive), 1600x1600, q=2-31, 200 kb/s, 45 fps, 100 tbn
    Metadata:
      encoder         : Lavc60.3.100 gif
frame=    1 fps=0.0 q=-0.0 size=       0kB time=00:00:00.02 bitrate=   0.0kbits/s speed=7.8e-05x    frame=   65 fps=0.3 q=-0.0 size=    4096kB time=00:00:01.44 bitrate=23301.7kbits/s speed=0.00561x    frame=  122 fps=0.5 q=-0.0 size=    8192kB time=00:00:02.71 bitrate=24763.4kbits/s speed=0.0105x    frame=  180 fps=0.7 q=-0.0 size=   12032kB time=00:00:04.00 bitrate=24641.5kbits/s speed=0.0155x    frame=  235 fps=0.9 q=-0.0 size=   16384kB time=00:00:05.22 bitrate=25712.2kbits/s speed=0.0202x    frame=  270 fps=1.0 q=-0.0 Lsize=   18878kB time=00:00:05.98 bitrate=25861.5kbits/s speed=0.0231x    
video:18878kB audio:0kB subtitle:0kB other streams:0kB global headers:0kB muxing overhead: 0.000103%
