import org.openrndr.application
import org.openrndr.color.ColorRGBa
import org.openrndr.draw.isolatedWithTarget
import org.openrndr.draw.renderTarget
import org.openrndr.extra.noise.random
import org.openrndr.extra.videoprofiles.gif
import org.openrndr.ffmpeg.ScreenRecorder
import org.openrndr.math.Vector3
import java.util.*
import kotlin.math.sin
import kotlin.math.sqrt

data class Vector(val x: Double = 0.0, val y: Double = 0.0, val z: Double = 0.0) {
    fun scale(scalar: Double): Vector = Vector(
        x = this.x * scalar,
        y = this.y * scalar,
        z = this.z * scalar
    )
}


fun main() = application {
    configure {
        width = 800
        height = 800
    }

    program {

        val scale = 1.0
        val rt = renderTarget(800, 800) {
            colorBuffer()
        }

        fun distance(a: Vector, b: Vector): Double {
            val x = a.x - b.x
            val y = a.y - b.y
            val z = a.z - b.z

            return sqrt(x * x + y * y + z * z)
        }

        fun middleVector(start: Vector, end: Vector): Vector {
            return Vector(
                x = start.x + 0.5 * (end.x - start.x),
                y = start.y + 0.5 * (end.y - start.y),
                z = start.z + 0.5 * (end.z - start.z)
            )
        }

        fun linearColor(d: Double): Double = (-255 / (600.0 * scale) * d + 255.0)

        var hasRun = false

        extend(ScreenRecorder()) {
            frameRate = 45
            maximumDuration = 6.0
            gif()
        }

        extend() {


            val vectors = listOf(
                Vector(sin(seconds) * 100, 70.0).scale(scale),
                Vector(sin(seconds) * 100 + 780.0, 70.0).scale(scale),
                Vector(sin(seconds) * 100 + 400.0, 730.0).scale(scale)
            )

            if (!hasRun) {
//                hasRun = false
//                rt.clearColor(0, ColorRGBa.BLACK)
                drawer.isolatedWithTarget(rt) {
                    var startVector = Vector(
                        x = random(0.0, 400.0),
                        y = random(0.0, 400.0),
                        z = random(0.0, 400.0)
                    )
                    val pointsAmount = 10_000

                    repeat(pointsAmount) {
                        val vectorTarget = vectors[Random().nextInt(vectors.size)]
                        startVector = middleVector(startVector, vectorTarget)

                        val dr = distance(startVector, vectors[0])
                        val r = linearColor(dr)
                        val dg = distance(startVector, vectors[1])
                        val g = linearColor(dg)
                        val db = distance(startVector, vectors[2])
                        val b = linearColor(db)

                        drawer.fill = ColorRGBa(r, g, b)

                        drawer.point(startVector.x, startVector.y)
                    }
                }
            }
        }

        extend() {

            drawer.translate(400.0, 400.0)

            drawer.rotate(
                rotationInDegrees = seconds * 30.0,
                axis = Vector3(0.0, 0.0, 200.0)
            )

            drawer.scale(0.3)

            drawer.image(rt.colorBuffer(0))
        }
    }
}

